package com.example.spring.demo.back.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.demo.back.entities.PersonaEntity;
import com.example.spring.demo.back.repository.PersonaRepository;

@Service
public class PersonaService {
	
	@Autowired
	private PersonaRepository personaRepository;
	
	public PersonaEntity savePersona(PersonaEntity persona) {
		return this.personaRepository.save(persona);
	}
	
	public List<PersonaEntity> getAllPersonas() {
		return this.personaRepository.findAll();
	}

}
