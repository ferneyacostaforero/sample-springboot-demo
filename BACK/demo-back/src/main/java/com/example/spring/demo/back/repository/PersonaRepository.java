package com.example.spring.demo.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.spring.demo.back.entities.PersonaEntity;

public interface PersonaRepository extends JpaRepository<PersonaEntity, Long>{

}
