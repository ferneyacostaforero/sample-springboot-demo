package com.example.spring.demo.back.security.repository;

import com.example.spring.demo.back.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findByEmail(String email);

    Optional<Usuario> findByUsernameOrEmail(String name, String email);

    List<Usuario> findByIdIn(List<Long> UsuarioIds);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
