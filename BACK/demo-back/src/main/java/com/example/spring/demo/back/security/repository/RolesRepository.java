package com.example.spring.demo.back.security.repository;

import com.example.spring.demo.back.entities.Rol;
import com.example.spring.demo.back.enumeration.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesRepository extends JpaRepository<Rol, Long> {

    Rol findByName(RoleName roleName);
}

