package com.example.spring.demo.back.enumeration;

public enum RoleName {
    ROLE_USER, ROLE_ADMIN
}
