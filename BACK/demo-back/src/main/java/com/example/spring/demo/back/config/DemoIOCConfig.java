package com.example.spring.demo.back.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.example.spring.demo.back")
public class DemoIOCConfig {

}
