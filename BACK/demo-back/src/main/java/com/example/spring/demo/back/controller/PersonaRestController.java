package com.example.spring.demo.back.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.spring.demo.back.entities.PersonaEntity;
import com.example.spring.demo.back.service.PersonaService;

@RestController
@RequestMapping("/api/persona/")
@CrossOrigin("*")
public class PersonaRestController {
	
	@Autowired
	private PersonaService personaService;
	
	@PostMapping("save")
	public ResponseEntity<PersonaEntity> savePersona(@RequestHeader(name = "Authorization") String bearerToken, @RequestBody PersonaEntity persona) {
		return ResponseEntity.ok(this.personaService.savePersona(persona));
	}
	
	@GetMapping("findAll")
	public List<PersonaEntity> getAll(@RequestHeader(name = "Authorization") String bearerToken) {
		return this.personaService.getAllPersonas();
	}
	

}
