package com.example.spring.demo.back.security.controller;

import com.example.spring.demo.back.entities.Rol;
import com.example.spring.demo.back.entities.Usuario;
import com.example.spring.demo.back.enumeration.RoleName;
import com.example.spring.demo.back.security.provider.JwtTokenProvider;
import com.example.spring.demo.back.security.repository.RolesRepository;
import com.example.spring.demo.back.security.repository.UsuarioRepository;
import com.example.spring.demo.back.security.response.ApiResponse;
import com.example.spring.demo.back.security.response.JwtAuthenticationResponse;
import com.example.spring.demo.back.security.response.LoginRequest;
import com.example.spring.demo.back.security.response.SignUpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

/**
 * @author ferney-acosta
 */
@RestController
@RequestMapping("/api/auth")
@CrossOrigin("*")
public class AuthRestController {


    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private UsuarioRepository userRepository;

    @Autowired
    private RolesRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    /**
     * autenticamos el usuario SPRING SECURITY CONTEXT
     *
     * @param request
     * @param loginRequest
     * @return
     */
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(HttpServletRequest request,
                                              @Valid @RequestBody LoginRequest loginRequest) {
        try {
            String usernameOrEmail = loginRequest.getUsernameOrEmail();
            String password = loginRequest.getPassword();

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(usernameOrEmail, password));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = tokenProvider.generateToken(authentication);

            return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(),
                    HttpStatus.NO_CONTENT);
        }
    }

    /**
     * creamos un nuevo usuario
     *
     * @param signUpRequest
     * @return
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        try {
            if (userRepository.existsByUsername(signUpRequest.getUsername())) {
                return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
            }

            if (userRepository.existsByEmail(signUpRequest.getEmail())) {
                return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
            }

            // Creating user's account
            Usuario user = new Usuario(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
                    signUpRequest.getPassword());

            user.setPassword(passwordEncoder.encode(user.getPassword()));

            Rol userRole = roleRepository.findByName(RoleName.ROLE_USER);

            user.setRoles(Collections.singletonList(userRole));

            Usuario result = userRepository.save(user);

            URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
                    .buildAndExpand(result.getUsername()).toUri();

            return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(),
                    HttpStatus.NO_CONTENT);
        }
    }
}

